from functions import *

url = 'https://theskylive.com/quickaccess?objects=moon-sun-mars-mercury-venus-jupiter-saturn-uranus-neptune&data=equatorial'
session = HTMLSession()
response = session.get(url)
keyInfoBox = response.html.find('.keyinfobox ar')

allCelestialBodies = getData(keyInfoBox)

#0 = A0
potentiometerPlanets = 0
#1 = A1
potentiometerAPI = 1
pinMode(potentiometerPlanets, "INPUT")
pinMode(potentiometerAPI, "INPUT")

statePlanets = 'Earth'
stateAPI = 'null'
textToPrint = 'null'
spaceCounter = ''

for t in allCelestialBodies:
    print("Name:", t.name)
    print("Right Ascension:", t.rightAscension)
    print("Declination:", t.declination)
    print("\n")

while True:
    setRGB(255, 105, 180)

    #0 assumes A0
    potentiometerPlanetsValue = analogRead(potentiometerPlanets)/1023*100 #Variable  interval: 0-100
    potentiometerAPIValue = analogRead(potentiometerAPI)/1023*100 #Variable interval: 0-100

    #Change state depending on potentiometerValuePlanets
    if(potentiometerPlanetsValue <= 100/9): #Divide by amount of planets
        statePlanets = 'Sun'
        spaceCounter = '     '
    elif(potentiometerPlanetsValue > 100/9 and potentiometerPlanetsValue < 100/9*2):
        statePlanets = 'Moon'
        spaceCounter = '    '
    elif (potentiometerPlanetsValue > 100/9*2 and potentiometerPlanetsValue < 100/9*3):
        statePlanets = 'Venus'
        spaceCounter = '   '
    elif (potentiometerPlanetsValue > 100/9*3 and potentiometerPlanetsValue < 100/9*4):
        statePlanets = 'Jupiter'
        spaceCounter = ' '
    elif (potentiometerPlanetsValue > 100/9*4 and potentiometerPlanetsValue < 100/9*5):
        statePlanets = 'Mars'
        spaceCounter = '    '
    elif(potentiometerPlanetsValue > 100/9*5 and potentiometerPlanetsValue < 100/9*6):
        statePlanets = 'Mercury'
        spaceCounter = ' '
    elif (potentiometerPlanetsValue > 100/9*6 and potentiometerPlanetsValue < 100/9*7):
        statePlanets = 'Saturn'
        spaceCounter = '  '
    elif (potentiometerPlanetsValue > 100/9*7 and potentiometerPlanetsValue < 100/9*8):
        statePlanets = 'Uranus'
        spaceCounter = '  '
    elif (potentiometerPlanetsValue >= 100/9*8):
        statePlanets = 'Neptune'
        spaceCounter = ' '

    #Change state depending on potentiometerAPIValue
    if(potentiometerAPIValue <= 100/7): #Divide by amount of values from API
        stateAPI = "englishName"
    if (potentiometerAPIValue > 100 / 7 and potentiometerAPI < 100/3*2):
        stateAPI = "gravity"
    if (potentiometerAPIValue > 100 / 7*2 and potentiometerAPI < 100/3*3):
        stateAPI = "density"
    if (potentiometerAPIValue > 100 / 7*3 and potentiometerAPI < 100/3*4):
        stateAPI = "meanRadius"
    if (potentiometerAPIValue > 100 / 7*4 and potentiometerAPI < 100/3*5):
        stateAPI = "eccentricity"
    if (potentiometerAPIValue > 100 / 7*5 and potentiometerAPI < 100/3*6):
        stateAPI = "inclination"
    if (potentiometerAPIValue >= 100 /7*6):  # Divide by amount of values from API
        stateAPI = "axialTilt"

    # request from API
    r = requests.get('https://api.le-systeme-solaire.net/rest/bodies/' + statePlanets)
    json = r.json()
    jsonValue = json[stateAPI]

    textToPrint = "Planet: " + str(statePlanets) + spaceCounter + str(stateAPI[:7].capitalize()) + ": " + str(jsonValue)
    print(textToPrint)

    setText_norefresh("{}".format(str(textToPrint)))