import requests
from requests_html import HTMLSession
import os

from grovepi import *
from grove_rgb_lcd import *
from classes import *

#Web-scraping, used to scrape TheSkyLive.com
def getData(keyInfoBox):
    celestialBodies = []
    i = 0
    j = 0

    for Body in ['The Sun', 'The Moon', 'Venus',
                 'Jupiter', 'Mars', 'Mercury',
                 'Saturn', 'Uranus', 'Neptune']:
        celestialBodies.append(CelestialBody())
        celestialBodies[i].name = Body
        celestialBodies[i].rightAscension = keyInfoBox[j].text
        j = j + 1
        celestialBodies[i].declination = keyInfoBox[j].text
        j = j + 1
        i = i + 1

    return celestialBodies
